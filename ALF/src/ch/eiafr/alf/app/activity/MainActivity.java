package ch.eiafr.alf.app.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import ch.eiafr.alf.R;
import ch.eiafr.alf.app.AlfAppli;
import ch.eiafr.alf.framework.segmentation.ISegmentationManager;

public class MainActivity extends Activity {

	
	public static AlfAppli alfControler;
	ISegmentationManager segmentationManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		alfControler = new AlfAppli(this.getApplicationContext());
		segmentationManager = alfControler.getSegmentationManagerInstance();
		
		final Button bTraining = (Button) findViewById(R.id.bTraining);
		bTraining.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				segmentationManager.setTrainingMode(true);
				Intent intent = new Intent(MainActivity.this, TrainingActivity.class);
				startActivity(intent);
			}
		});
		
		final Button bOnline = (Button) findViewById(R.id.bOnline);
		bOnline.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				segmentationManager.setTrainingMode(false);
				Intent intent = new Intent(MainActivity.this, OnlineActivity.class);
				startActivity(intent);
			}
		});
	}
}
