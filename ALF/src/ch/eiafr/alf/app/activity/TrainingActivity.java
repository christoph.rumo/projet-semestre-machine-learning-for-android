package ch.eiafr.alf.app.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import ch.eiafr.alf.R;
import ch.eiafr.alf.app.AlfAppli;


public class TrainingActivity extends Activity {

	private boolean started = false;

	private Button bStartStop;
	private Button bSave;
	private Button bTrain;
	private Button bTestTrainingFile;
	private Button bTestKFoldCrossValidationModulo, bTestKFoldCrossValidationRandom;
	private ProgressBar pb;
	public static TextView textViewResult, textViewResult2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.training);
		
		textViewResult = (TextView) findViewById(R.id.textViewResult);
		textViewResult2 = (TextView) findViewById(R.id.textViewResult2);


		pb = (ProgressBar) findViewById(R.id.progressBarTraining);

		bSave = (Button) findViewById(R.id.buttonSave);
		bSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlfAppli.saveTrainingDatasIntoFile();
			}
		});
		

		bStartStop = (Button) findViewById(R.id.buttonStartStop);
		bStartStop.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!started) {
					started = true;
					bStartStop.setText("STOP");
					pb.setVisibility(1);
					;
					bSave.setEnabled(false);
				} else {
					started = false;
					bStartStop.setText("START");
					pb.setVisibility(4);
					;
					bSave.setEnabled(true);
				}
				AlfAppli.setStarted(started);
			}
		});

	
	bTrain = (Button) findViewById(R.id.buttonTrain);
	bTrain.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			AlfAppli.trainSVM();
		}
	});
	
	bTestKFoldCrossValidationModulo = (Button) findViewById(R.id.ButtonTestKFoldCrossValidationModulo);
	bTestKFoldCrossValidationModulo.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			AlfAppli.testKFoldCrossValidationModulo();
		}
	});
	
	bTestKFoldCrossValidationRandom = (Button) findViewById(R.id.ButtonTestKFoldCrossValidationRandom);
	bTestKFoldCrossValidationRandom.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			AlfAppli.testKFoldCrossValidationRandom();
		}
	});
	
	bTestTrainingFile = (Button) findViewById(R.id.buttonTestTrainingFile);
	bTestTrainingFile.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			AlfAppli.testTrainingFile();
		}
	});
	}
	public void onRadioButtonClicked(View view) {
		boolean checked = ((RadioButton) view).isChecked();
		switch (view.getId()) {
		case R.id.radio_move2:
			if (checked)
				AlfAppli.setKindMove(2);
			break;
		case R.id.radio_move1:
			if (checked)
				AlfAppli.setKindMove(1);
			break;
		case R.id.radio_move3:
			if (checked)
				AlfAppli.setKindMove(3);
			break;
		}
		
	}

}
