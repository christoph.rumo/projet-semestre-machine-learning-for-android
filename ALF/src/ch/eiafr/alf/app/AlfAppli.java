package ch.eiafr.alf.app;

import java.io.IOException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;

import android.content.Context;
import ch.eia.fr.sensor.AE_Sensor;
import ch.eiafr.alf.app.activity.OnlineActivity;
import ch.eiafr.alf.app.activity.TrainingActivity;
import ch.eiafr.alf.framework.classify.ClassifyManager;
import ch.eiafr.alf.framework.datamining.IMiningManager;
import ch.eiafr.alf.framework.datamining.MiningManager;
import ch.eiafr.alf.framework.segmentation.ISegmentationManager;
import ch.eiafr.alf.framework.segmentation.SegmentationManager;
import ch.eiafr.alf.framework.segmentation.data.CSVManager;
import ch.eiafr.alf.framework.segmentation.data.TrainingManager;

public class AlfAppli extends Thread {

	public static Context applicationContext;

	private static CSVManager csvManager;
	private static ClassifyManager classifyManager;
	private static SegmentationManager segmentationManager;
	private static TrainingManager trainingManager;
	public static IMiningManager miningManager;

	public static final int WALK = 2;
	public static final int RUN = 1;
	public static final int STAY = 3;

	public static final int FREQUENZ = 400;

	public AlfAppli(Context applicationContext) {
		AlfAppli.applicationContext = applicationContext;
		init();
	}

	public void init() {
		// Datamining
		trainingManager = TrainingManager.getInstance();
		trainingManager.addClassType("run", RUN);
		trainingManager.addClassType("walk", WALK);
		trainingManager.addClassType("stay", STAY);

		// CSV
		csvManager = new CSVManager(applicationContext);

		segmentationManager = new SegmentationManager(trainingManager, csvManager);
		segmentationManager.setClassType(WALK);

		// Data
		miningManager = new MiningManager(segmentationManager);
		miningManager.setWindowSize(2);
		
		for (AE_Sensor s : miningManager.getAvaibleSensors()) {
			if (s.getName().equals(MiningManager.SENSOR_ACCELEROMETER)
					|| s.getName().equals(MiningManager.SENSOR_GYROSCOPE)
					) {
				miningManager.addSensor(s, s.getName(), FREQUENZ);
			} else if (s.getName().equals(MiningManager.SENSOR_GPS)) {
//				 miningManager.addSensor(new
//				 AndroidSensorFactory().createSensor(MiningManager.SENSOR_GPS),
//				 MiningManager.SENSOR_GPS, FREQUENZ);
			}
		}
		// for (AE_Sensor s : miningManager.getAvaibleSensors()){
		// miningManager.addSensor(s, s.getName(), FREQUENZ);
		// }

		segmentationManager.setMiningManager(miningManager);
		segmentationManager.setMaxBufferSize(100);
		segmentationManager.setIgnoreCounter(0);

		// Classify
		classifyManager = new ClassifyManager(csvManager);
		classifyManager.setCurrentKernelType(classifyManager.KERNEL_TYPE_SIGMOID);
	}

	// From GUI
	public static void classify() {
			ArrayList<String> resultList = null;
			try {
				classifyManager.normalizeProceed(csvManager.getOnlinePath(), csvManager.getNormalizedOnlinePath(), true);
				resultList = classifyManager.classify(csvManager.getNormalizedOnlinePath());
//				resultList = classifyManager.classify(csvManager.getOnlinePath());
				csvManager.clearOnlineData();
				OnlineActivity.textViewResult.setText("");
				for (String result : resultList) {
					int kind = (int) Double.parseDouble(result);
					switch (kind) {
					case RUN:
						OnlineActivity.textViewResult.append("  1");
						break;
					case WALK:
						OnlineActivity.textViewResult.append("  2");
						break;
					case STAY:
						OnlineActivity.textViewResult.append("  3");
						break;
					default:
						OnlineActivity.textViewResult.append("Classify ERROR");
						break;
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	// From GUI
	public static void saveTrainingDatasIntoFile() {
		segmentationManager.saveTrainingDataIntoFile(csvManager.getTrainingPath(), true);
	}

	// From GUI
	public static void writeOnlineData() {
		segmentationManager.saveOnlineDataIntoFile(csvManager.getOnlinePath(), false);
	}

	// From GUI
	public static void testTrainingFile() {
		double successRate = 0.0;
		try {
			successRate = classifyManager.testTrainingFile(csvManager.getNormalizedTrainingPath());
			TrainingActivity.textViewResult.setText("Training file test: Success rate: " + successRate);
		} catch (Exception e) {
			TrainingActivity.textViewResult.setText("" + e);
			e.printStackTrace();
		}

	}

	// From GUI
	static int kFoldNumberModulo = 2;
	public static void testKFoldCrossValidationModulo() {
		double successRate = 0.0;
		try {
			successRate = classifyManager.testKFoldCrossValidation(kFoldNumberModulo, false, csvManager.getTrainingPath());
			TrainingActivity.textViewResult.setText("K-Fold Number: "+kFoldNumberModulo+", KFoldCrossValidation MODULO \nsuccessrate: " + successRate);
		} catch (Exception e) {
			TrainingActivity.textViewResult.setText("" + e);
			e.printStackTrace();
		}
		kFoldNumberModulo++;
	}
	
	// From GUI
	static int kFoldNumberRandom = 2;
	public static void testKFoldCrossValidationRandom() {
		double successRate = 0.0;
		try {
			successRate = classifyManager.testKFoldCrossValidation(kFoldNumberRandom, true, csvManager.getNormalizedTrainingPath());
			TrainingActivity.textViewResult2.setText("K-Fold Number: "+kFoldNumberRandom+", KFoldCrossValidation RANDOM \nsuccessrate: " + successRate);
		} catch (Exception e) {
			TrainingActivity.textViewResult2.setText("" + e);
			e.printStackTrace();
		}
		kFoldNumberRandom++;
	}
	
	//Not in use
	public static void importSVM(){
		try {
			classifyManager.importSVM(applicationContext.getExternalCacheDir()+"/back.ser");
		} catch (StreamCorruptedException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Not in use
	public static void exportSVM(){
		try {
			classifyManager.exportSVM(applicationContext.getExternalCacheDir()+"/back.ser");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// From GUI
	public static void trainSVM() {
		try {
			kFoldNumberModulo = 2;
			kFoldNumberRandom = 2;
			
			classifyManager.normalizeDefineMinMax(csvManager.getTrainingPath());
			classifyManager.normalizeProceed(csvManager.getTrainingPath(), csvManager.getNormalizedTrainingPath(), false);
			Double error = classifyManager.trainSVM(csvManager.getNormalizedTrainingPath());
//			Double error = classifyManager.trainSVM(csvManager.getTrainingPath());
			
			TrainingActivity.textViewResult.setText("Train SVM error: " + error);
			TrainingActivity.textViewResult2.setText("");
		} catch (Exception e) {
			TrainingActivity.textViewResult.setText("" + e);
			e.printStackTrace();
		}
	}


	// From GUI
	public static void setStarted(boolean started) {
		if (started) {
			segmentationManager.startCapture();
		} else {
			segmentationManager.stopCapture();
		}
	}

	// From GUI
	public static void setKindMove(int kindOfMove) {
		segmentationManager.setClassType(kindOfMove);
	}

	public ISegmentationManager getSegmentationManagerInstance() {
		return segmentationManager;
	}

}
