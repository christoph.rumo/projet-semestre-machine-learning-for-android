package ch.eiafr.alf.framework.segmentation.data;

import java.util.ArrayList;

public class OnlineDataBean {

	private ArrayList<String> data = new ArrayList<String>();

	public void addRecord(String currentRecord) {
		data.add(currentRecord);
	}

	public String getRecord(int index) {
		return data.get(index);
	}

	public ArrayList<String> getRecords() {
		return data;
	}
}
