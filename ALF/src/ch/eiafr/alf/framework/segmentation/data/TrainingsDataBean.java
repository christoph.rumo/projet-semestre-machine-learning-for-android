package ch.eiafr.alf.framework.segmentation.data;

import java.util.ArrayList;

public class TrainingsDataBean {
	
	private int classeNumber;
	private String classeName;
	private ArrayList<String> resultList = new ArrayList<String>();
	
	public TrainingsDataBean(String classeName, int classeNumber) {
		this.classeName = classeName;
		this.classeNumber = classeNumber;
	}
	
	public void addRecord(String record){
		resultList.add(record);
	}
	
	public void resetData() {
		resultList = new ArrayList<String>();
	}
	
	public int getClassNumber() {
		return classeNumber;
	}
	public void setClassNumber(int classeNumber) {
		this.classeNumber = classeNumber;
	}

	public String getClassName() {
		return classeName;
	}
	public void setClassName(String classeName) {
		this.classeName = classeName;
	}

	public ArrayList<String> getResultList() {
		return resultList;
	}


	
}
