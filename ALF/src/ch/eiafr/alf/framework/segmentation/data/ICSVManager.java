package ch.eiafr.alf.framework.segmentation.data;

import java.io.IOException;
import java.util.logging.Level;

public interface ICSVManager {

	/**
	 * Get the path of training captures .csv backup file in device storage 
	 */
	public String getTrainingPath();
	
	/**
	 * Get the path of online captures .csv backup file in device storage
	 */
	public String getOnlinePath();
	
	/**
	 * set training file path
	 */
	public void setTrainingFilePath(String path);
	
	/**
	 * set online file path
	 */
	public void setOnlineFilePath(String path);
	
	/**
	 * clear online data
	 */
	public void clearOnlineData() throws IOException;
	
	/**
	 * delete all the temp files
	 */
	public void deleteTempFiles();
	/**
	 * Get the logger level
	 */
	public Level getLoggerLevel();

	/**
	 * Get the logger level
	 * @param Level level
	 */
	public void setLoggerLevel(Level level);
	
}
