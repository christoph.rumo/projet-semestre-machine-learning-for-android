package ch.eiafr.alf.framework.segmentation.data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import android.content.Context;

public class CSVManager implements ICSVManager {

	private BufferedWriter bw;
	private FileWriter fw;
	private String normalizedTrainingPath;
	private String normalizedOnlinePath;
	private String onlinePath;
	private String trainingPath;

	private Context applicationContext;

	private final static Logger LOGGER = Logger.getLogger(CSVManager.class.getName());

	public CSVManager(Context applicationContext) {
		this.applicationContext = applicationContext;
		File externalCacheDir = applicationContext.getExternalCacheDir();
		onlinePath = externalCacheDir + "/generated_onlineData.csv";
		trainingPath = externalCacheDir + "/generated_trainingData.csv";
		normalizedTrainingPath = externalCacheDir + "/generated_normalizedTrainingData.csv";
		normalizedOnlinePath = externalCacheDir + "/generated_normalizedOnlineData.csv";
		LOGGER.finest("CSVManager created");
		deleteTempFiles();
	}

	public void saveTrainingIntoFile(ArrayList<TrainingsDataBean> trainingdataBeanList, boolean eraseOldData)
			throws IOException {
		LOGGER.finest("CSVManager: saveTrainingIntoFile start");
		File file = new File(trainingPath);

		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		fw = new FileWriter(file.getAbsoluteFile(), eraseOldData);
		bw = new BufferedWriter(fw);

		for (TrainingsDataBean trainingsDataBean : trainingdataBeanList) {
			for (String result : trainingsDataBean.getResultList()) {
				bw.write(result + "," + trainingsDataBean.getClassNumber());
				bw.newLine();
			}

		}
		bw.close();
		LOGGER.finest("CSVManager: saveTrainingIntoFile finish");
	}

	public void saveOnlineDataIntoFile(OnlineDataBean onlineData, boolean eraseOldData) throws IOException {
		LOGGER.finest("CSVManager: saveOnlineDataIntoFile start");
		File file = new File(onlinePath);
		file.createNewFile();
		file.deleteOnExit();

		fw = new FileWriter(file.getAbsoluteFile(), eraseOldData);
		bw = new BufferedWriter(fw);

		for (int i = 0; i < onlineData.getRecords().size(); i++) {
			bw.write("" + onlineData.getRecord(i));
			LOGGER.finest("CSVManager: saveOnlineDataIntoFile, record: " + onlineData.getRecord(i));
			bw.newLine();
		}
		bw.close();
		LOGGER.finest("CSVManager: saveOnlineDataIntoFile finish");
	}

	public static String getFirstTrainingFileLine(String path) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
		String line = bufferedReader.readLine();
		bufferedReader.close();
		LOGGER.finest("CSVManager: getFirstTrainingFileLine " + line);
		return line;
	}

	public static int getInputNumber(String path) throws Exception {
		int inputNumber = 0;
		String line = "";
		line = getFirstTrainingFileLine(path);
		int pos = line.indexOf(',');
		while (pos != -1) {
			inputNumber++;
			line = line.substring(pos + 1);
			pos = line.indexOf(',');
		}
		inputNumber++;
		LOGGER.info("CSVManager: getInputNumber " + inputNumber);
		return inputNumber;
	}

	/**
	 * Split training file into numberOfKFoldFile files using modulo or random
	 * repartition
	 * 
	 * @param trainingFilePath
	 */
	public void splitTrainingFile(int numberOfKFoldFile, boolean randomDistribution, String trainingFilePath)
			throws Exception {
		long startTime = System.currentTimeMillis();
		File splitTrainingFile = null;
		for (int i = 1; i <= numberOfKFoldFile; i++) {
			splitTrainingFile = new File(getSplitFilePath(i));
			if (splitTrainingFile.exists()) {
				splitTrainingFile.delete();
			}
			splitTrainingFile.createNewFile();
			splitTrainingFile.deleteOnExit();
		}

		// lecture du fichier texte
		InputStream ips = new FileInputStream(trainingFilePath);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader br = new BufferedReader(ipsr);

		FileWriter fw = null;
		BufferedWriter bw = null;

		String record;
		if (randomDistribution) {
			Random rand = new Random();
			while ((record = br.readLine()) != null) {
				int randomNum = rand.nextInt(numberOfKFoldFile) + 1;
				splitTrainingFile = new File(getSplitFilePath(randomNum));
				fw = new FileWriter(splitTrainingFile, true);
				bw = new BufferedWriter(fw);
				bw.write(record);
				bw.newLine();
				bw.close();
			}
		} else {
			int i = 0;
			while ((record = br.readLine()) != null) {
				String path = getSplitFilePath(((i + 1) % numberOfKFoldFile) + 1);
				splitTrainingFile = new File(path);
				fw = new FileWriter(splitTrainingFile, true);
				bw = new BufferedWriter(fw);
				bw.write(record);
				bw.newLine();
				bw.close();
				i++;
			}

		}
		br.close();
		long endTime = System.currentTimeMillis();
		LOGGER.warning("Total elapsed time in execution of method splitTrainingFile() :"
				+ (endTime - startTime) + "ms, numberOfKFoldFile: " + numberOfKFoldFile
				+ ", randomDistribution: " + randomDistribution);
	}

	public void clearOnlineData() throws IOException {
		File onlineData = new File(onlinePath);
		if (onlineData.exists()) {
			onlineData.delete();
		}
		onlineData.createNewFile();
	}

	public void deleteTempFiles() {
		File normalizedTrainingFile = new File(normalizedTrainingPath);
		if (normalizedTrainingFile.exists()) {
			normalizedTrainingFile.delete();
			LOGGER.warning("Deleted file: " + normalizedTrainingFile);
		}
		File normalizedOnlineFile = new File(normalizedOnlinePath);
		if (normalizedOnlineFile.exists()) {
			normalizedOnlineFile.delete();
			LOGGER.warning("Deleted file: " + normalizedOnlineFile);
		}
		File onlineData = new File(onlinePath);
		if (onlineData.exists()) {
			onlineData.delete();
			LOGGER.warning("Deleted file: " + onlineData);
		}
		for (int i = 0; i < 200; i++) {
			File splitFile = new File(getSplitFilePath(i));
			if (splitFile.exists()) {
				splitFile.delete();
				LOGGER.warning("Deleted file: " + splitFile);
			}
		}
	}

	public String getNormalizedTrainingPath() {
		return normalizedTrainingPath;
	}

	public String getSplitFilePath(int number) {
		return applicationContext.getExternalCacheDir() + "/generated_splitFile" + number + ".csv";
	}

	public String getOnlinePath() {
		return onlinePath;
	}

	public String getTrainingPath() {
		return trainingPath;
	}

	public void setTrainingFilePath(String path) {
		this.trainingPath = path;
	}

	public void setOnlineFilePath(String path) {
		this.onlinePath = path;
	}

	public Level getLoggerLevel() {
		return LOGGER.getLevel();
	}

	public void setLoggerLevel(Level level) {
		LOGGER.setLevel(level);
	}

	public String getNormalizedOnlinePath() {
		return normalizedOnlinePath;
	}

	public void setNormalizedOnlinePath(String normalizedOnlinePath) {
		this.normalizedOnlinePath = normalizedOnlinePath;
	}

	public void setNormalizedTrainingPath(String normalizedTrainingPath) {
		this.normalizedTrainingPath = normalizedTrainingPath;
	}

	public void setOnlinePath(String onlinePath) {
		this.onlinePath = onlinePath;
	}

	public void setTrainingPath(String trainingPath) {
		this.trainingPath = trainingPath;
	}

}
