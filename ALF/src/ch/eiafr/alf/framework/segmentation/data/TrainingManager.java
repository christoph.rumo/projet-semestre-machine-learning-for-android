package ch.eiafr.alf.framework.segmentation.data;

import java.util.ArrayList;

public class TrainingManager {

	private static TrainingManager instance;
	private ArrayList<TrainingsDataBean> trainingData = new ArrayList<TrainingsDataBean>();

	/* Constructor *******************************************************************************/
	private TrainingManager(){}
	
	/* Methods ***********************************************************************************/
	public static TrainingManager getInstance(){
		if(instance == null){
			instance = new TrainingManager();
		}
		return instance;
	}
	public void addClassType(String classeName, int classeValue) {
		trainingData.add(new TrainingsDataBean(classeName, classeValue));
	}

	public TrainingsDataBean getTrainingDataBean(int classeValue) {
		for (TrainingsDataBean trainingsDataBean : trainingData) {
			if (trainingsDataBean.getClassNumber() == classeValue) {
				return trainingsDataBean;
			}
		}
		return null;
	}

	public TrainingsDataBean getTrainingDataBean(String classeName) {
		for (TrainingsDataBean trainingsDataBean : trainingData) {
			if (trainingsDataBean.getClassName().equals(classeName)) {
				return trainingsDataBean;
			}
		}
		return null;
	}

	public ArrayList<TrainingsDataBean> getTrainingDataBeans() {
		return trainingData;
	}

	public void resetTrainingData() {
		for (TrainingsDataBean trainingsDataBean : trainingData) {
			trainingsDataBean.resetData();
		}
	}

}
