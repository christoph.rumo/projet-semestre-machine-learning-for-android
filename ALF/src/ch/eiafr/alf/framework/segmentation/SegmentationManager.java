package ch.eiafr.alf.framework.segmentation;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import ch.eiafr.alf.framework.classify.NormalizationUntil;
import ch.eiafr.alf.framework.datamining.IMiningManager;
import ch.eiafr.alf.framework.datamining.MiningManager;
import ch.eiafr.alf.framework.segmentation.data.CSVManager;
import ch.eiafr.alf.framework.segmentation.data.OnlineDataBean;
import ch.eiafr.alf.framework.segmentation.data.TrainingManager;

public class SegmentationManager implements ISegmentationManager {

	private final static Logger LOGGER = Logger.getLogger(SegmentationManager.class.getName());

	private TrainingManager dataManager;
	private IMiningManager miningManager;
	private CSVManager csvManager;
	private OnlineDataBean onlineData = new OnlineDataBean();
	private boolean started = false; // true if capture is started
	private int classNbr = 1; // classNbr refer to number indicate the label
								// (ex: 1,2,3,...)
	private boolean trainingMode = true; // indicate if training or online mode
	private String currentRecord = "";
	private int countRecord = 0;
	private int countRecordInMemory = 0;
	private int currentSensorIndex = 0;
	private int totalRecordCount = 0; // counter of record into the buffer
	private int pedometerStepsCounter = 0;
	private int ignoreCounter = 0; // ignore each x sensor input
	private int currentIgnoreCounter = 0;
	private int maxBufferSize = 100; // maximal record line before write
	private long startTime = System.currentTimeMillis();

	/*
	 * Constructor
	 * **************************************************************
	 * ****************
	 */
	public SegmentationManager(TrainingManager dataManager, CSVManager csvManager) {
		this.dataManager = dataManager;
		this.csvManager = csvManager;
		LOGGER.finest("SegmentationManager created");
	}

	/*
	 * Methods
	 * ******************************************************************
	 * ****************
	 */
	public void update(Object output, String nSensor) {
		if (started) {
			int enabledSensorsNumber = miningManager.getEnabledSensor().size();
			LOGGER.finest(output + " Receive data from " + nSensor + ", currentSensor: "
					+ miningManager.getEnabledSensor().get(currentSensorIndex));
			if (miningManager.getEnabledSensor().get(currentSensorIndex)
					.equals(MiningManager.SENSOR_ACCELEROMETER)
					&& nSensor.equals(MiningManager.SENSOR_ACCELEROMETER)) {
				if (ignoreCounter <= currentIgnoreCounter) {
					if (countRecord != 0) {
						currentRecord += ",";
					}
					currentIgnoreCounter = 0;
					currentRecord += output.toString();
					countRecord++;
					currentSensorIndex = (currentSensorIndex + 1) % enabledSensorsNumber;
				} else {
					currentIgnoreCounter++;
					LOGGER.severe("Ignore counter " + currentIgnoreCounter + "/" + ignoreCounter);
				}
			} else if (miningManager.getEnabledSensor().get(currentSensorIndex)
					.equals(MiningManager.SENSOR_GYROSCOPE)
					&& nSensor.equals(MiningManager.SENSOR_GYROSCOPE)) {
				if (ignoreCounter <= currentIgnoreCounter) {
					if (countRecord != 0) {
						currentRecord += ",";
					}
					currentIgnoreCounter = 0;
					currentRecord += output.toString();
					countRecord++;
					currentSensorIndex = (currentSensorIndex + 1) % enabledSensorsNumber;
				} else {
					currentIgnoreCounter++;
					LOGGER.severe("Ignore counter " + currentIgnoreCounter + "/" + ignoreCounter);
				}
			} else if (miningManager.getEnabledSensor().get(currentSensorIndex)
					.equals(MiningManager.SENSOR_COMPAS)
					&& nSensor.equals(MiningManager.SENSOR_COMPAS)) {
				if (ignoreCounter <= currentIgnoreCounter) {
					if (countRecord != 0) {
						currentRecord += ",";
					}
					currentRecord += output.toString();
					countRecord++;
					currentSensorIndex = (currentSensorIndex + 1) % enabledSensorsNumber;
				} else {
					currentIgnoreCounter++;
					LOGGER.severe("Ignore counter " + currentIgnoreCounter + "/" + ignoreCounter);
				}
			} else if (nSensor.equals(MiningManager.SENSOR_PEDOMETER)) {
				pedometerStepsCounter++;
			}
			if (miningManager.getEnabledSensor().get(currentSensorIndex)
					.equals(MiningManager.SENSOR_PEDOMETER)) {
				if (countRecord != 0) {
					currentRecord += ",";
				}
				currentRecord += "" + pedometerStepsCounter;
				pedometerStepsCounter = 0;
				countRecord++;
				currentSensorIndex = (currentSensorIndex + 1) % miningManager.getEnabledSensor().size();
			}
			if (countRecord == (miningManager.getWindowSize() * miningManager.getEnabledSensor().size())) {
				if (trainingMode) {
					dataManager.getTrainingDataBean(classNbr).addRecord(currentRecord);
					countRecordInMemory++;
					totalRecordCount++;
					// time counter
					long endTime = System.currentTimeMillis();
					//
					LOGGER.info("\nRecord added \nbuffer: " + countRecordInMemory + "/" + maxBufferSize
							+ " \ntotal records: " + totalRecordCount + "\ntime to full this record: "
							+ (endTime - startTime) + "ms" + "\n record: " + currentRecord);

					startTime = System.currentTimeMillis();
					if (countRecordInMemory >= maxBufferSize) {
						LOGGER.warning("Max buffer reach! Add training data into file");
						saveTrainingDataIntoFile(csvManager.getTrainingPath(), true);
					}

				} else {
					countRecordInMemory++;
					onlineData.addRecord(currentRecord);
					saveOnlineDataIntoFile(csvManager.getOnlinePath(), true);
					LOGGER.info("Record online data: " + currentRecord);
				}
				countRecord = 0;
				currentRecord = "";
			}
		}
	}

	@Override
	public void setMiningManager(IMiningManager sensorManager) {
		this.miningManager = sensorManager;
	}

	@Override
	public void setClassType(int move) {
		this.classNbr = move;
	}

	@Override
	public void startCapture() {
		started = true;
		System.out.println("Start capture");
	}

	@Override
	public void stopCapture() {
		started = false;
		System.out.println("Stop capture");
	}

	@Override
	public void setStarted(boolean started) {
		this.started = started;
		if (started)
			System.out.println("Start capture");
		else
			System.out.println("Stop capture");
	}

	@Override
	public boolean isStarted() {
		return started;
	}

	@Override
	public void resetTrainingData() {
		dataManager.resetTrainingData();
	}

	@Override
	public void resetOnlineData() {
		onlineData = new OnlineDataBean();
	}

	@Override
	public void setIgnoreCounter(int ignoreCounter) {
		this.ignoreCounter = ignoreCounter;
	}

	@Override
	public int getIgnoreCounter() {
		return ignoreCounter;
	}

	@Override
	public void setMaxBufferSize(int maxBufferSize) {
		this.maxBufferSize = maxBufferSize;
	}

	@Override
	public int getMaxBufferSize() {
		return maxBufferSize;
	}

	@Override
	public void setTrainingMode(boolean status) {
		countRecord = 0;
		currentRecord = "";
		this.trainingMode = status;
	}

	@Override
	public void saveTrainingDataIntoFile(String path, boolean eraseOldData) {
		try {
			csvManager.setTrainingFilePath(path);
			csvManager.saveTrainingIntoFile(dataManager.getTrainingDataBeans(), eraseOldData);
			resetTrainingData();
			countRecordInMemory = 0;
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void saveOnlineDataIntoFile(String path, boolean eraseOldData) {
		try {
			csvManager.setOnlineFilePath(path);
			csvManager.saveOnlineDataIntoFile(onlineData, eraseOldData);
			resetOnlineData();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Level getLoggerLevel() {
		return LOGGER.getLevel();
	}

	@Override
	public void setLoggerLevel(Level level) {
		LOGGER.setLevel(level);
	}
}
