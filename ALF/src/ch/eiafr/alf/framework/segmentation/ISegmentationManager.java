package ch.eiafr.alf.framework.segmentation;

import java.util.logging.Level;

import ch.eiafr.alf.framework.datamining.IMiningManager;

public interface ISegmentationManager {

	
	/**
	 * Set a given MiningManager
	 */
	public void setMiningManager(IMiningManager miningManager);
	
	/**
	 * Set the class number being to be captured
	 */
	public void setClassType(int classType);
	
	/**
	 * Start a Training/Online capture
	 */
	public void startCapture();
	
	/**
	 * Stop Training/Online capture
	 */
	public void stopCapture();
	
	/**
	 * Set manually value of start/stop capture
	 * @param boolean : start when true and stop when stop
	 */
	public void setStarted(boolean started);
	
	/**
	 * Get start/stop status
	 */
	public boolean isStarted();
	
	/**
	 * Reset all training pro label data containers
	 */
	public void resetTrainingData();
	
	/**
	 * Reset online captured data
	 */
	public void resetOnlineData();
		
	/**
	 * Ignore each nth capture
	 * @param int ignoreCounter
	 */
	public void setIgnoreCounter(int ignoreCounter);
	
	/**
	 * Get IgnoreCounter value
	 */
	public int getIgnoreCounter();
	
	/**
	 * Set the maximal size of captures buffer
	 * @param int maxBufferSize
	 */
	public void setMaxBufferSize(int maxBufferSize);
	
	/**
	 * Get size of captures buffer
	 */
	public int getMaxBufferSize();
		
	/**
	 * Set the if segmentation manager is in training mode or online mode
	 */
	public void setTrainingMode(boolean status);
	
	/**
	 * Save training captures into a file
	 * @param String path, boolean eraseOldData
	 */
	public void saveTrainingDataIntoFile(String path, boolean eraseOldData);
	
	/**
	 * Save Online captures into a file
	 * @param String path, boolean eraseOldData
	 */
	public void saveOnlineDataIntoFile(String path, boolean eraseOldData);
	
	/**
	 * Get the logger level
	 */
	public Level getLoggerLevel();

	/**
	 * Get the logger level
	 * @param Level level
	 */
	public void setLoggerLevel(Level level);
	
}
