package ch.eiafr.alf.framework.classify;

import java.io.IOException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.logging.Level;

import org.encog.ml.svm.KernelType;
import org.encog.ml.svm.SVM;

public interface IClassifyManager {
	
	/**
	 * Train the framework using the default training file
	 * @throws IOException 
	 */
	public double trainSVM() throws Exception;

	/**
	 * Train the framework using a given training file
	 * @throws IOException
	 * @param String : file path
	 */
	double trainSVM(String trainingFilePath) throws Exception;
	
	/**
	 * Train the framework using a given training file
	 * @throws IOException
	 * @param String : file path
	 * @param SVM : svm
	 */
	double trainSVM(SVM svm, String trainingFilePath) throws Exception;
	
	/**
	 * Classify the online file
	 * @throws IOException
	 */
	ArrayList<String> classify() throws Exception;
	/**
	 * Classify the online file
	 * @throws IOException
	 * @param String : file path 
	 * @param SVM svm
	 */
	public ArrayList<String> classify(String onlineFilePath, SVM svm) throws Exception;	
	/**
	 * Classify the online file
	 * @throws IOException
	 * @param String : file path 
	 */
	public ArrayList<String> classify(String onlineFilePath) throws Exception;
	
	/**
	 * Launch the test of the last training dataSet
	 * @throws IOException 
	 */
	public double testTrainingFile(String trainingFilePath) throws Exception;
	
	/**
	 * Launch the K-Fold Cross-Validation test of the last training dataSet
	 * @throws IOException 
	 */
	public double testKFoldCrossValidation(int kFoldNumber, boolean randomDistribution, String path) throws Exception;
	
	/**
	 * Set Gamma value for support vector machine
	 */
	public void setGamma(double gamma);
	
	/**
	 * Get the actual gamma value
	 */
	public double getGamma();
	
	/**
	 * Set C value for support vector machine
	 */
	public void setC(double c);
	
	/**
	 * Get the actual C value
	 */
	public double getC();
	
	/**
	 * Set fold value for support vector machine
	 */
	public void setFold(int fold);
	
	/**
	 * Get the actual Fold
	 */
	public int getFold();
	
	/**
	 * Get the current type of kernel used for support vector machine
	 */
	public KernelType getCurrentKernelType();
	
	/**
	 * Set a new type of kernel for support vector machine
	 * @param KernelType : new kernel to set
	 */
	public void setCurrentKernelType(KernelType currentKernelType);

	/**
	 * Export the current svm object into a file
	 * @param filepath
	 * @throws IOException
	 */
	public void exportSVM(String filepath) throws IOException;

	/**
	 * import a svm object from a file
	 * @param filepath
	 * @throws StreamCorruptedException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void importSVM(String filepath) throws StreamCorruptedException, IOException, ClassNotFoundException;
	/**
	 * Get the logger level
	 */
	public Level getLoggerLevel();

	/**
	 * Get the logger level
	 * @param Level level
	 */
	public void setLoggerLevel(Level level);
	
	/**
	 * Return the current SVM object
	 */	
	public SVM getCurrentSvm();
	/**
	 * Define normal range of value for a given training data set
	 * @param inputFilePath
	 * @throws Exception
	 */
	public void normalizeDefineMinMax(String inputFilePath) throws Exception;
	/**
	 * proceed with normalization on a given training data set 
	 * @param InputFilePath
	 * @param outputFilePath
	 * @throws Exception
	 */
	public void normalizeProceed(String InputFilePath, String outputFilePath, boolean onlineMode) throws Exception;
	
}
