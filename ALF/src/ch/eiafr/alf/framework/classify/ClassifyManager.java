package ch.eiafr.alf.framework.classify;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.encog.Encog;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.svm.KernelType;
import org.encog.ml.svm.SVM;
import org.encog.ml.svm.SVMType;
import org.encog.ml.svm.training.SVMSearchTrain;
import org.encog.plugin.system.SystemLoggingPlugin;
import org.encog.util.csv.CSVFormat;
import org.encog.util.logging.EncogLogging;
import org.encog.util.simple.TrainingSetUtil;

import ch.eiafr.alf.framework.segmentation.data.CSVManager;

public class ClassifyManager implements IClassifyManager {

	public final KernelType KERNEL_TYPE_RADIAL_BASIS_FUNCTION = KernelType.RadialBasisFunction;
	public final KernelType KERNEL_TYPE_LINEAR = KernelType.Linear;
	public final KernelType KERNEL_TYPE_POLY = KernelType.Poly;
	public final KernelType KERNEL_TYPE_SIGMOID = KernelType.Sigmoid;

	private final int MAX_IDENTIC_ERROR_NUMBER = 250;

	private CSVManager csvManager;
	private SVMSearchTrain svmTrain;
	private ClassifyTester classifyTester;
	private KernelType currentKernelType = KERNEL_TYPE_RADIAL_BASIS_FUNCTION;
	private SVM svm;
	private NormalizationUntil normalizationUntil;

	
	private final static Logger LOGGER = Logger.getLogger(ClassifyManager.class.getName());

	public ClassifyManager(CSVManager csvManager) {
		this.csvManager = csvManager;
		classifyTester = new ClassifyTester(this, csvManager);
		SystemLoggingPlugin logging = (SystemLoggingPlugin) Encog.getInstance().getLoggingPlugin();
		logging.setLogLevel(EncogLogging.LEVEL_INFO);
		logging.startConsoleLogging();
		LOGGER.finest("ClassifyManager created");
	}

	public double trainSVM() throws Exception {
		return trainSVM(csvManager.getTrainingPath());
	}
	
	public double trainSVM(String trainingFilePath) throws Exception {
		svm = new SVM(CSVManager.getInputNumber(trainingFilePath) - 1, SVMType.NewSupportVectorClassification,
				currentKernelType);
		return trainSVM(svm, trainingFilePath);
	}

	@Override
	public double trainSVM(SVM svm, String trainingFilePath) throws Exception {
		int inputNumber = CSVManager.getInputNumber(trainingFilePath);
	    LOGGER.info("Start trainSVM trainingFile:" + trainingFilePath+", inputnumber: " + inputNumber);
		final MLDataSet trainingSet = TrainingSetUtil.loadCSVTOMemory(CSVFormat.ENGLISH, trainingFilePath,
				false, inputNumber - 1, 1);

		// TRAIN the SVMs
		svmTrain = new SVMSearchTrain(svm, trainingSet);
		
		int epoch = 1;
		int identicCounter = 0;
		double oldValue = 0.0;
		long startTime = System.currentTimeMillis();
		do {
			long startTime2 = System.currentTimeMillis();
			svmTrain.iteration();
			if (oldValue == svmTrain.getError()) {
				identicCounter++;
			    LOGGER.finest("trainSVM - identicCounter:" + identicCounter);
			} else {
				identicCounter = 0;
			}
			oldValue = svmTrain.getError();
			long endTime2 = System.currentTimeMillis();
		    LOGGER.info("trainSVM() #" + epoch + " iteration time :" + (endTime2 - startTime2)
					+ "ms, Error:" + svmTrain.getError());
			epoch++;
		} while (svmTrain.getError() > 0.01 && identicCounter < MAX_IDENTIC_ERROR_NUMBER);
		long endTime = System.currentTimeMillis();
	    LOGGER.warning("Total elapsed time in execution of method trainSVM(): "
				+ (endTime - startTime) + "ms, error: "+svmTrain.getError()+", total epoch: "+(epoch-1));
	    if(identicCounter >= MAX_IDENTIC_ERROR_NUMBER){
		    LOGGER.severe("Exit the trainSVM iteration method, too many identic error value: " + svmTrain.getError());
	    }
		if (svmTrain.getError() > 0.01) {
			System.out.println();
		    LOGGER.severe("Not able to finish the svmTrain error: " + svmTrain.getError());
		}
	    LOGGER.finest("Finish trainSVM");
		return svmTrain.getError();
	}

	@Override
	public ArrayList<String> classify() throws Exception {
		return classify(csvManager.getOnlinePath());
	}
	
	@Override
	public ArrayList<String> classify(String onlineFilePath, SVM svm) throws Exception {
		return classify(csvManager.getOnlinePath());
	}

	@Override
	public ArrayList<String> classify(String onlineFilePath) throws Exception {
	    LOGGER.finest("Start classify");
		int inputNumber = CSVManager.getInputNumber(onlineFilePath);

		final MLDataSet onlineSet = TrainingSetUtil.loadCSVTOMemory(CSVFormat.ENGLISH, onlineFilePath, false,
				inputNumber, 0);

		ArrayList<String> listResult = new ArrayList<String>();

		for (MLDataPair pair : onlineSet) {
			final MLData output = svm.compute(pair.getInput());
		    LOGGER.warning("Classify: output=" + output.getData(0));
			listResult.add("" + output.getData(0));
		}
		Encog.getInstance().shutdown();
	    LOGGER.finest("Finish classify");
		return listResult;
	}

	@Override
	public double testTrainingFile(String trainingFilePath) throws Exception {
		return classifyTester.testClassifyTrainingData(svm, trainingFilePath);
	}

	public double testKFoldCrossValidation(int kFoldNumber, boolean randomDistribution, String trainingFilePath) throws Exception {
		return classifyTester.testKFoldCrossValidation(kFoldNumber, randomDistribution, trainingFilePath);
	}

	@Override
	public KernelType getCurrentKernelType() {
		return currentKernelType;
	}

	@Override
	public void setCurrentKernelType(KernelType currentKernelType) {
		this.currentKernelType = currentKernelType;
	}

	@Override
	public void setGamma(double gamma) {
		svmTrain.setBestGamma(gamma);
	}

	@Override
	public void setC(double c) {
		svmTrain.setBestConst(c);
	}

	@Override
	public void setFold(int fold) {
		svmTrain.setFold(fold);
	}

	@Override
	public double getGamma() {
		return svmTrain.getBestGamma();
	}

	@Override
	public double getC() {
		return svmTrain.getBestConst();
	}

	@Override
	public int getFold() {
		return svmTrain.getFold();
	}
	
	public Level getLoggerLevel(){
		return LOGGER.getLevel();
	}
	
	public void setLoggerLevel(Level level){
		LOGGER.setLevel(level);
	}

	public SVM getCurrentSvm() {
		return svm;
	}
	
	public void exportSVM(String filepath) throws IOException{
			File file = new File(filepath);
			final FileOutputStream fichier = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fichier);
			oos.writeObject(svm);
			LOGGER.warning("ClassifyManager: SVM sucessful export to: "+ filepath);
	}
	
	public void importSVM(String filepath) throws StreamCorruptedException, IOException, ClassNotFoundException{
			File file = new File(filepath);
			final FileInputStream fichier = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fichier);
			svm = (SVM) ois.readObject();
			LOGGER.warning("ClassifyManager: SVM sucessful inport from: "+ filepath);

	}
	
	@Override
	public void normalizeDefineMinMax(String InputFilePath) throws Exception {
		normalizationUntil = new NormalizationUntil(CSVManager.getInputNumber(InputFilePath) - 1);
		normalizationUntil.calculateColomnMinMax(InputFilePath);
	}

	@Override
	public void normalizeProceed(String InputFilePath, String outputFilePath, boolean onlineMode)
			throws Exception {
		normalizationUntil.proceed(InputFilePath, outputFilePath, onlineMode);
	}

}
