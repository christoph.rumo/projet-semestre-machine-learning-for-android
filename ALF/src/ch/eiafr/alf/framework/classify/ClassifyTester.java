package ch.eiafr.alf.framework.classify;

import java.util.logging.Logger;

import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.svm.SVM;
import org.encog.ml.svm.SVMType;
import org.encog.util.csv.CSVFormat;
import org.encog.util.simple.TrainingSetUtil;

import ch.eiafr.alf.framework.segmentation.data.CSVManager;

public class ClassifyTester {

	private CSVManager csvManager;
	private ClassifyManager classifyManager;

	private int numberOfKFoldFile;

	private final static Logger LOGGER = Logger.getLogger(ClassifyManager.class.getName());

	public ClassifyTester(ClassifyManager classifyManager, CSVManager csvManager) {
		this.classifyManager = classifyManager;
		this.csvManager = csvManager;
	}

	public double testClassifyTrainingData(SVM svm, String trainingFilePath) throws Exception {
		LOGGER.finest("ClassifyTester testClassifyTrainingData() start");
		int inputNumber = CSVManager.getInputNumber(trainingFilePath);

		int successCount = 0;
		int totalCount = 0;

		final MLDataSet trainingSet = TrainingSetUtil.loadCSVTOMemory(CSVFormat.ENGLISH, trainingFilePath,
				false, inputNumber - 1, 1);

		for (MLDataPair pair : trainingSet) {
			final MLData output = svm.compute(pair.getInput());
			if (pair.getIdeal().getData(0) != output.getData(0)) {
				LOGGER.severe("testClassifyTrainingData, ideal: " + pair.getIdeal().getData(0)
						+ " != found: " + output.getData(0) + " fail: " + (totalCount - successCount) + "/"
						+ totalCount);
			} else {
				LOGGER.info("testClassifyTrainingData, ideal: " + pair.getIdeal().getData(0) + " == found: "
						+ output.getData(0)+ " fail: " + (totalCount - successCount) + "/"
						+ totalCount);
				successCount++;
			}
			totalCount++;
		}
		double successRate = ((double) successCount * 100) / (double) totalCount;
		LOGGER.warning("ClassifyTester testClassifyTrainingData() finish, sucess rate: " + successRate);
		return successRate;
	}

	public double testKFoldCrossValidation(int kFoldNumber, boolean randomDistribution, String trainingFilePath) throws Exception {
		LOGGER.finest("ClassifyTester testKFoldCrossValidation() start");

		csvManager.splitTrainingFile(kFoldNumber, randomDistribution, trainingFilePath);

		int inputNumber = CSVManager.getInputNumber(trainingFilePath);

		int successCount = 0;
		int totalCount = 0;
		for (int i = 1; i <= kFoldNumber; i++) {
			SVM svm = new SVM(inputNumber - 1, SVMType.NewSupportVectorClassification,
					classifyManager.getCurrentKernelType());
			double error = classifyManager.trainSVM(svm, csvManager.getSplitFilePath(i));
			LOGGER.warning("testKFoldCrossValidation(), trainSVM error: " + error + ", training file: "
					+ csvManager.getSplitFilePath(i));
			for (int j = 1; j <= kFoldNumber; j++) {
				if (i != j) {
					final MLDataSet trainingSet = TrainingSetUtil.loadCSVTOMemory(CSVFormat.ENGLISH,
							csvManager.getSplitFilePath(j), false, inputNumber - 1, 1);

					for (MLDataPair pair : trainingSet) {
						final MLData output = svm.compute(pair.getInput());
						if (pair.getIdeal().getData(0) != output.getData(0)) {
							LOGGER.severe("testKFoldCrossValidation(), ideal: " + pair.getIdeal().getData(0)
									+ " != found: " + output.getData(0) + " fail: "
									+ (totalCount - successCount) + "/" + totalCount);
						} else {
							LOGGER.info("testClassifyTrainingData, ideal: " + pair.getIdeal().getData(0)
									+ " == found: " + output.getData(0)+ " fail: " + (totalCount - successCount) + "/"
									+ totalCount);
							successCount++;
						}
						totalCount++;
					}
				}
			}

		}
		double successRate = ((double) successCount * 100) / (double) totalCount;
		LOGGER.severe("ClassifyTester testKFoldCrossValidation() finish, sucessRate: " + successRate);
		return successRate;
	}

	public int getNumberOfKFoldFile() {
		return numberOfKFoldFile;
	}

	public void setNumberOfKFoldFile(int numberOfKFoldFile) {
		this.numberOfKFoldFile = numberOfKFoldFile;
	}

}
