package ch.eiafr.alf.framework.classify;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

public class NormalizationUntil {
	private double[] dataHigh;
	private double[] dataLow;
	private double normalizedHigh;
	private double normalizedLow;
	private int numberColomn;
	private int lineCounter;

	private final static Logger LOGGER = Logger.getLogger(NormalizationUntil.class.getName());

	/**
	 * Construct the normalization utility. Default to normalization range of -1
	 * to +1.
	 * 
	 * @param dataHigh
	 *            The high value for the input data.
	 * @param dataLow
	 *            The low value for the input data.
	 * @throws IOException
	 */
	public NormalizationUntil(int numberColomn) throws IOException {
		this(numberColomn, -1, 1);
	}

	/**
	 * Construct the normalization utility, allow the normalization range to be
	 * specified.
	 * 
	 * @param dataHigh
	 *            The high value for the input data.
	 * @param dataLow
	 *            The low value for the input data.
	 * @param dataHigh
	 *            The high value for the normalized data.
	 * @param dataLow
	 *            The low value for the normalized data.
	 * @throws IOException
	 */
	public NormalizationUntil(int numberColomn, double normalizedHigh, double normalizedLow)
			throws IOException {
		dataHigh = new double[numberColomn];
		dataLow = new double[numberColomn];
		this.numberColomn = numberColomn;
		this.normalizedHigh = normalizedHigh;
		this.normalizedLow = normalizedLow;
	}

	public void calculateColomnMinMax(String inputFilePath) throws IOException {
		String record;
		BufferedReader br = new BufferedReader(new FileReader(inputFilePath));
		while ((record = br.readLine()) != null) {
			String[] columns = record.split(",");
			for (int i = 0; i < numberColomn; i++) {
				try {
					double value = Double.valueOf(columns[i]);
					if (dataHigh[i] < value) {
						dataHigh[i] = value;
					} else if (dataLow[i] > value) {
						dataLow[i] = value;
					}
				} catch (Exception e) {
					LOGGER.severe("" + e);
					System.out.print(e);
				}
			}
		}
		br.close();
		for (int i = 0; i < numberColomn; i++) {
			LOGGER.warning("NormalisationUntil: Colomn #" + i + ", max: " + dataHigh[i] + ", min: "
					+ dataLow[i]);
		}
	}

	public void proceed(String inputFilePath, String outputFilePath, boolean onlineMode) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(inputFilePath));
		File normalizedFile = new File(outputFilePath);
		FileWriter fw = new FileWriter(normalizedFile, false);
		BufferedWriter bw = new BufferedWriter(fw);
		
		lineCounter = 0;
		String record;
		try {
			while ((record = br.readLine()) != null) {
				lineCounter++;
				String[] columns = record.split(",");
				boolean first = true;
				for (int i = 0; i < numberColomn; i++) {
					if (!first) {
						bw.write(",");
					} else {
						first = false;
					}
					double value = Double.valueOf(columns[i]);
					double normal = normalize(i, value);
					LOGGER.finest("NormalizationUntil: proceed, inputValue: " + value + ", normlized value: "
							+ normal);
					bw.write("" + normal);
				}
				if (!onlineMode) {
					bw.write("," + columns[numberColomn]);
				}
				bw.write("\n");
			}
			LOGGER.warning("NormalizationUntil: proceed " + inputFilePath + ", number of line : "
					+ lineCounter);
		} catch (Exception e) {
			LOGGER.severe("" + e);
		}
		br.close();
		bw.close();
	}

	/**
	 * Normalize x.
	 * 
	 * @param x
	 *            The value to be normalized.
	 * @return The result of the normalization.
	 */
	public double normalize(int colomnIndex, double x) {
		return ((x - dataLow[colomnIndex]) / (dataHigh[colomnIndex] - dataLow[colomnIndex]))
				* (normalizedHigh - normalizedLow) + normalizedLow;
	}

	/**
	 * Denormalize x.
	 * 
	 * @param x
	 *            The value to denormalize.
	 * @return The denormalized value.
	 */
	public double denormalize(int colomnIndex, double x) {
		return ((dataLow[colomnIndex] - dataHigh[colomnIndex]) * x - normalizedHigh * dataLow[colomnIndex] + dataHigh[colomnIndex]
				* normalizedLow)
				/ (normalizedLow - normalizedHigh);
	}

	public int getLineCounter() {
		return lineCounter;
	}

}
