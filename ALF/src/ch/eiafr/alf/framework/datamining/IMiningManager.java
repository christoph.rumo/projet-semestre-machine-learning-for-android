package ch.eiafr.alf.framework.datamining;

import java.util.ArrayList;
import java.util.logging.Level;

import ch.eia.fr.sensor.AE_Sensor;

public interface IMiningManager {

	/**
	 * Add a new sensor to the list of sensors
	 * @param AE_Sensor
	 * @param String : name of sensor
	 * @param int : frequenz of mesures
	 */
	public void addSensor(AE_Sensor sensor, String name, int frequenz);
	
	/**
	 * Remove a Sensor from list
	 * @param String : name of sensor
	 */
	public void removeSensor(String name);
	
	/**
	 * Get the list of available sensors of the device
	 */
	public ArrayList<AE_Sensor> getAvaibleSensors();
	
	/**
	 * Get the list of active sensors
	 */
	public ArrayList<String> getEnabledSensor();
	
	/**
	 * Set the windows size : number of captures pro window
	 */
	public void setWindowSize(int size);
	
	/**
	 * Get size of a window
	 */
	public int getWindowSize();
	
	/**
	 * set the frequency of a sensor with given name
	 * @param String : name of sensor
	 * @param int : frequency
	 */
	public void setSensorFrequency(int frequency,String sensorName);
	/**
	 * Get the logger level
	 */
	public Level getLoggerLevel();
	/**
	 * Set the logger level
	 * @param Level level
	 */
	public void setLoggerLevel(Level level);
	
}
