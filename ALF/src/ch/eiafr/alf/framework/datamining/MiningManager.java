package ch.eiafr.alf.framework.datamining;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import android.content.Context;
import android.os.Handler;
import ch.eia.fr.factory.AndroidSensorFactory;
import ch.eia.fr.manager.AE_SensorManager;
import ch.eia.fr.sensor.AE_Sensor;
import ch.eia.fr.sensor.AE_SensorListener;
import ch.eiafr.alf.app.AlfAppli;
import ch.eiafr.alf.framework.segmentation.SegmentationManager;

public class MiningManager implements AE_SensorListener,IMiningManager {
	
	// Constants types of sensor
	public final static String SENSOR_ACCELEROMETER = "Accelerometer sensor";
	public final static String SENSOR_GYROSCOPE = "Gyroscope sensor";
	public final static String SENSOR_COMPAS = "Compas sensor";
	public final static String SENSOR_GPS = "Gps sensor";
	public final static String SENSOR_PEDOMETER = "Pedometer sensor";

	// Attributs
	private AE_SensorManager sensorsManager;
	private SegmentationManager segmentationManager;
	private ArrayList<AE_Sensor> sensorList;
	private ArrayList<String> sensorEnableList = new ArrayList<String>();
	private Context context;
	private Handler handler = null;
	private int windowsSize;
	
	private final static Logger LOGGER = Logger.getLogger(MiningManager.class.getName());

	/* Constructor *******************************************************************************/
	public MiningManager(SegmentationManager segmentationManager) {
		handler = new Handler();
		context = AlfAppli.applicationContext;
		this.segmentationManager = segmentationManager;
		AndroidSensorFactory asf = new AndroidSensorFactory();
		sensorList = asf.createSensors();
		sensorsManager = AE_SensorManager.getInstance();
		sensorsManager.setManager((android.hardware.SensorManager) context
				.getSystemService(Context.SENSOR_SERVICE));
		sensorsManager
				.setLocationManager((android.location.LocationManager) context
						.getSystemService(Context.LOCATION_SERVICE));
		sensorsManager.addListener(this);
		LOGGER.finest("MiningManager created");
	}
	
	/* Methods ***********************************************************************************/
	public void addSensor(AE_Sensor sensor, String name, int frequency){
		LOGGER.severe("MiningManager add sensor: "+ name+", frequency: "+frequency);
		sensorsManager.addSensor(sensor, name, frequency);
		sensorEnableList.add(name);
	}
	
	public ArrayList<AE_Sensor> getAvaibleSensors(){
		return sensorList;
	}
	
	public ArrayList<String> getEnabledSensor(){
		return sensorEnableList;
	}
	
	public void removeSensor(String name){
		LOGGER.warning("MiningManager remove sensor: "+ name);
		sensorsManager.removeSensor(name);
	}
	
	public int getWindowSize(){
		return windowsSize;
	}
	
	/* Override Methods **************************************************************************/
	@Override
	public void update(Object output, String nSensor) {
		segmentationManager.update(output, nSensor);
	}

	@Override
	public Handler getHandler() {
		return handler;
	}

	@Override
	public void setWindowSize(int size) {
		this.windowsSize = size;	
		LOGGER.warning("MiningManager set WindowSize: "+size);
	}
	
	@Override
	public void setSensorFrequency(int frequency,String sensorName) {
		removeSensor(sensorName);
		for(AE_Sensor s : getAvaibleSensors()){
			if(s.getName().equals(sensorName)){
				addSensor(s, sensorName, frequency);
			}
		}
		LOGGER.warning("MiningManager change sensor frequency: "+ sensorName+", frequency: "+frequency);
	}
	
	@Override
	public Level getLoggerLevel() {
		return LOGGER.getLevel();
	}

	@Override
	public void setLoggerLevel(Level level) {
		LOGGER.setLevel(level);
	}
}
