package ch.eiafr.alf.ml;

import java.util.ArrayList;

public class TrainingsData {
	
	private ArrayList<PedometerBean> lpbWalk = new ArrayList<PedometerBean>();
	private ArrayList<PedometerBean> lpbRun = new ArrayList<PedometerBean>();
	private ArrayList<PedometerBean> lpbStay = new ArrayList<PedometerBean>();
	
	public static final int WindowsMilis = 30000;
	
	
	public void addTraining(PedometerBean pb, int move){
		switch(move){
			case 1:
				lpbWalk.add(pb);
				break;
			case 2: 
				lpbRun.add(pb);
				break;
			case 3: 
				lpbStay.add(pb);
				break;
		}
	}
	
	public PedometerBean getTraining(int index, int kindMove){
		switch(kindMove){
		case 1: return lpbWalk.get(index);
		case 2: return lpbRun.get(index);
		case 3: return lpbStay.get(index);
		default : return null;
		}
	}
	
	public ArrayList<PedometerBean> getTrainings(int kindMove){
		switch(kindMove){
		case 1: return lpbWalk;
		case 2: return lpbRun;
		case 3: return lpbStay;
		default : return null;
		}
	}

	
}
