package ch.eiafr.alf.ml;

import java.util.ArrayList;

public class OnlineData {

	private ArrayList<PedometerBean> data = new ArrayList<PedometerBean>();

	public static final int WindowsMilis = 5000;

	public void addOnlineData(PedometerBean pb) {
		data.add(pb);

	}

	public PedometerBean getOnlineData(int index) {
		return data.get(index);
	}

	public ArrayList<PedometerBean> getOnlineDatas() {
		return data;
	}

}
