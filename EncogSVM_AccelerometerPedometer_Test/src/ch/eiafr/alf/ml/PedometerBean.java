package ch.eiafr.alf.ml;


public class PedometerBean {
	
	private int nbrSteps;
	
	public int getNbrSteps() {
		return nbrSteps;
	}
	public void setNbrSteps(int nbrSteps) {
		this.nbrSteps = nbrSteps;
	}
	public void addStep(){
		nbrSteps++;
	}
}
