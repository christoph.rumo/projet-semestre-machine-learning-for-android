package ch.eiafr.alf.ml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.encog.Encog;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.svm.SVM;
import org.encog.ml.svm.training.SVMSearchTrain;
import org.encog.util.csv.CSVFormat;
import org.encog.util.simple.TrainingSetUtil;

import android.content.Context;

public class MLTraining {

	private TrainingsData trainingsData;
	private OnlineData onlineData;
	private BufferedWriter bw;
	private FileWriter fw;
	private String onlinePath;
	private String trainingPath;

	private static final int WALK = 1;
	private static final int RUN = 2;
	private static final int STAY = 3;

	public MLTraining(TrainingsData trainingsData, OnlineData onlineData,
			Context context) {
		this.onlineData = onlineData;
		this.trainingsData = trainingsData;
		onlinePath = context.getExternalCacheDir()
				+ "/generated_onlineData.csv";
		trainingPath = context.getExternalCacheDir()
				+ "/generated_trainingData.csv";
	}

	public void saveTraining() throws IOException {
		File file = new File(trainingPath);
		if(!file.exists()){
			file.createNewFile();
		}

		fw = new FileWriter(file,true);
		bw = new BufferedWriter(fw);

		for (int i = 0; i < trainingsData.getTrainings(WALK).size(); i++) {
			PedometerBean pb = trainingsData.getTraining(i, WALK);
			bw.write(pb.getNbrSteps() + "," + "1");
			bw.newLine();
			System.out.println("write walk");
		}

		for (int i = 0; i < trainingsData.getTrainings(RUN).size(); i++) {
			PedometerBean pb = trainingsData.getTraining(i, RUN);
			bw.write(pb.getNbrSteps() + "," + "2");
			bw.newLine();
			System.out.println("write run");
		}

		for (int i = 0; i < trainingsData.getTrainings(STAY).size(); i++) {
			PedometerBean pb = trainingsData.getTraining(i, STAY);
			bw.write(pb.getNbrSteps() + "," + "3");
			bw.newLine();
			System.out.println("write stay");
		}
		bw.close();
	}

	public void recordData() throws IOException {
		File file = new File(onlinePath);
		file.createNewFile();

		FileWriter fw2 = new FileWriter(file,true );
		BufferedWriter bw2 = new BufferedWriter(fw2);

		for (int i = 0; i < onlineData.getOnlineDatas().size(); i++) {
			PedometerBean pb = onlineData.getOnlineData(i);
			bw2.write("" + pb.getNbrSteps());
			bw2.newLine();
		}

		bw2.close();
	}

	public void classify() throws IOException {

		final int INPUT = 1; // number of features
		// final int IDEAL = 2; //number of labels, usually = 1
		/**
		 * The main method.
		 * 
		 * @param args
		 *            No arguments are used.
		 */
		final MLDataSet trainingSet = TrainingSetUtil.loadCSVTOMemory(
				CSVFormat.ENGLISH, trainingPath, false, INPUT, 1);
		final MLDataSet onlineSet = TrainingSetUtil.loadCSVTOMemory(
				CSVFormat.ENGLISH, onlinePath, false, INPUT, 0);

		// Example Extract one row:
		// MLDataPair result=((BasicMLDataSet)trainingSet).getData().get(0);

		// Creation of the SVM
		SVM svm = new SVM(INPUT, false);

		// TRAIN the SVMs
		SVMSearchTrain train = new SVMSearchTrain(svm, trainingSet);

		int epoch = 1;
		do {
			train.iteration();
			System.out
					.println("Epoch #" + epoch + " Error:" + train.getError());
			epoch++;
		} while (train.getError() > 0.01);

		// CLASSIFICATION: test the trained SVM
		System.out.println("SVM Results:");
		for (MLDataPair pair : onlineSet) {
			final MLData output = svm.compute(pair.getInput());
			System.out.println(pair.getInput().getData(0) + ", actual="
					+ output.getData(0));
		}
		System.out.println("DONE");
		Encog.getInstance().shutdown();
	}
}
