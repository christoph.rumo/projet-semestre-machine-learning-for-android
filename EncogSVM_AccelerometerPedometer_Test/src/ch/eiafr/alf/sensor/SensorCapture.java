package ch.eiafr.alf.sensor;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.os.Handler;
import ch.eia.fr.ae_framework.Constants;
import ch.eia.fr.factory.AndroidSensorFactory;
import ch.eia.fr.manager.AE_SensorManager;
import ch.eia.fr.sensor.AE_Sensor;
import ch.eia.fr.sensor.AE_SensorListener;
import ch.eiafr.alf.activity.MainActivity;
import ch.eiafr.alf.ml.MLTraining;
import ch.eiafr.alf.ml.OnlineData;
import ch.eiafr.alf.ml.PedometerBean;
import ch.eiafr.alf.ml.TrainingsData;

public class SensorCapture implements AE_SensorListener {

	/**
	 * Sensors manager
	 */
	private AE_SensorManager sensorsManager;
	private TrainingsData trainingsData = new TrainingsData();
	private OnlineData onlineData = new OnlineData();
	private PedometerBean pb = new PedometerBean();
	private MLTraining mlTraining;

	private boolean started = false;
	private String recordType = "";
	private int move = 0;
	private Context context;
	private Long startTime;

	private static final int FREQUENZ = 200;

	public SensorCapture() {
		context = MainActivity.ma.getApplicationContext();
		mlTraining = new MLTraining(trainingsData, onlineData, context);
		AndroidSensorFactory asf = new AndroidSensorFactory();
		ArrayList<AE_Sensor> sensors = asf.createSensors();
		sensorsManager = AE_SensorManager.getInstance();
		sensorsManager.setManager((android.hardware.SensorManager) context
				.getSystemService(Context.SENSOR_SERVICE));
		sensorsManager
				.setLocationManager((android.location.LocationManager) context
						.getSystemService(Context.LOCATION_SERVICE));
		for (AE_Sensor s : sensors)
			sensorsManager.addSensor(s, s.getName(), FREQUENZ);
		sensorsManager.addListener(this);
	}

	@Override
	public Handler getHandler() {
		return null;
	}

	@Override
	public void update(Object output, String nSensor) {
		if (started) {
			if (System.currentTimeMillis() - startTime > TrainingsData.WindowsMilis) {
				if (getRecordType().equals("training")) {
					trainingsData.addTraining(pb, move);
				} else if (getRecordType().equals("online")) {
					onlineData.addOnlineData(pb);
				}
				pb = new PedometerBean();
				startTime = System.currentTimeMillis();
			} else if (nSensor.equals(Constants.PEDOMETER)) {
				pb.addStep();
			}
		}

	}

	public void save() {
		try {
			mlTraining.saveTraining();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void classify() {
		try {
			mlTraining.classify();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void record() {
		try {
			mlTraining.recordData();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
		if (started)
			startTime = System.currentTimeMillis();
	}

	public void setKindMove(int move) {
		this.move = move;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

}
