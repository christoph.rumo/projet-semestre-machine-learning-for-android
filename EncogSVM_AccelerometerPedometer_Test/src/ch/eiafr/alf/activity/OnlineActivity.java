package ch.eiafr.alf.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import ch.eiafr.alf.ALF.R;

public class OnlineActivity extends Activity {

	//private static SensorCapture sc;

	private Button bClassify;
	private Button bRecord;
	private ProgressBar pb;
	private boolean started = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.online);
		
		pb = (ProgressBar) findViewById(R.id.progressBar1);
		
		bClassify = (Button) findViewById(R.id.buttonClassify);
		bClassify.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainActivity.sc.classify();
			}
		});
		
		bRecord = (Button) findViewById(R.id.buttonRecord);
		bRecord.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!started) {
					MainActivity.sc.setRecordType("online");
					started = true;
					pb.setVisibility(1);
					
				} else {
					started = false;
					pb.setVisibility(4);
					MainActivity.sc.record();
				}
				MainActivity.sc.setStarted(started);
			}
		});



	}
}
