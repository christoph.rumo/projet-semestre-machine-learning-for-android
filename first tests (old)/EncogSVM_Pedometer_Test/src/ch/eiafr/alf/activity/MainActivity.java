package ch.eiafr.alf.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import ch.eiafr.alf.ALF.R;
import ch.eiafr.alf.sensor.SensorCapture;

public class MainActivity extends Activity {

	public static MainActivity ma;
	public static SensorCapture sc;

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ma = this;
		sc = new SensorCapture();
		sc.setKindMove(1);
		final Button bTraining = (Button) findViewById(R.id.bTraining);
		final Button bOnline = (Button) findViewById(R.id.bOnline);
		bTraining.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, TrainingActivity.class);
				startActivity(intent);
			}
		});

		bOnline.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, OnlineActivity.class);
				startActivity(intent);
			}
		});
	}
}
