package ch.eiafr.alf.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import ch.eiafr.alf.ALF.R;

public class TrainingActivity extends Activity {

	private boolean started = false;

	//private static SensorCapture sc;
	private Button bStartStop;
	private Button bSave;
	private ProgressBar pb;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.training);

		pb = (ProgressBar) findViewById(R.id.progressBar1);

		bSave = (Button) findViewById(R.id.buttonSave);
		bSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainActivity.sc.save();
			}
		});
		

		bStartStop = (Button) findViewById(R.id.buttonStartStop);
		bStartStop.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!started) {
					MainActivity.sc.setRecordType("training");
					started = true;
					bStartStop.setText("STOP");
					pb.setVisibility(1);
					;
					bSave.setEnabled(false);
				} else {
					started = false;
					bStartStop.setText("START");
					pb.setVisibility(4);
					;
					bSave.setEnabled(true);
				}
				MainActivity.sc.setStarted(started);
			}
		});

	}

	public void onRadioButtonClicked(View view) {
		boolean checked = ((RadioButton) view).isChecked();
		switch (view.getId()) {
		case R.id.radio_run:
			if (checked)
				MainActivity.sc.setKindMove(2);
			break;
		case R.id.radio_walk:
			if (checked)
				MainActivity.sc.setKindMove(1);
			break;
		case R.id.radio_stay:
			if (checked)
				MainActivity.sc.setKindMove(3);
			break;
		}
		
	}

}
