package ch.eiafr.alf.sensor;

import java.util.ArrayList;

import android.content.Context;
import android.os.Handler;
import ch.eia.fr.factory.AndroidSensorFactory;
import ch.eia.fr.manager.AE_SensorManager;
import ch.eia.fr.sensor.AE_Sensor;
import ch.eia.fr.sensor.AE_SensorListener;
import ch.eiafr.alf.controler.AlfControler;

public class SensorManager implements AE_SensorListener {

	private static final int FREQUENZ = 30000;

	private AE_SensorManager sensorsManager;

	private Context context;

	public SensorManager() {
		context = AlfControler.applicationContext;

		AndroidSensorFactory asf = new AndroidSensorFactory();
		ArrayList<AE_Sensor> sensors = asf.createSensors();
		
		sensorsManager = AE_SensorManager.getInstance();
		sensorsManager.setManager((android.hardware.SensorManager) context
				.getSystemService(Context.SENSOR_SERVICE));
		sensorsManager
				.setLocationManager((android.location.LocationManager) context
						.getSystemService(Context.LOCATION_SERVICE));
		
		for (AE_Sensor s : sensors)
			sensorsManager.addSensor(s, s.getName(), FREQUENZ);
		sensorsManager.addListener(this);
	}

	@Override
	public void update(Object output, String nSensor) {
		AlfControler.update(output, nSensor);
	}



	@Override
	public Handler getHandler() {
		return null;
	}
}
