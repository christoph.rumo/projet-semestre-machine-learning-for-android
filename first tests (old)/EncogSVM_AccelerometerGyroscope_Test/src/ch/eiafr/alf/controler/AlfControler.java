package ch.eiafr.alf.controler;

import java.io.IOException;

import android.content.Context;
import ch.eiafr.alf.ml.MLEncog;
import ch.eiafr.alf.ml.data.CSVManager;
import ch.eiafr.alf.ml.data.RecordManager;
import ch.eiafr.alf.sensor.SensorManager;

public class AlfControler {
	
	public static SensorManager sensorCapture;
	public static Context applicationContext;
	
	private static CSVManager dataManager;
	private static MLEncog mlEncog;
	private static RecordManager recordManager;
	
	public static boolean trainingMode = true;


	public AlfControler(Context applicationContext) {
		AlfControler.applicationContext = applicationContext;
		sensorCapture = new SensorManager();
		recordManager = new RecordManager();
		recordManager.setKindMove(1);
		dataManager = new CSVManager(applicationContext);
		mlEncog = new MLEncog(dataManager);
	}
	
	public static void saveTrainingDatasIntoFile() {
		try {
			dataManager.saveTrainingIntoFile(recordManager.getTrainingsData());
			recordManager.resetTrainingData();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeOnlineData() {
		try {
			dataManager.saveOnlineDataIntoFile(recordManager.getOnlineData());
			recordManager.resetOnlineData();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void classify() {
		try {
			mlEncog.classify();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void trainSVM() {
		try {
			//mlEncog.normalizeData();
			mlEncog.trainSVM();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void update(Object output, String nSensor) {
		recordManager.update(output, nSensor);
	}
	
	public static void setStarted(boolean started){
		recordManager.setStarted(started);
	}

	public static void setKindMove(int kindOfMove) {
		recordManager.setKindMove(kindOfMove);
	}
	
}
