package ch.eiafr.alf.activity;

import ch.eiafr.alf.R;
import ch.eiafr.alf.controler.AlfControler;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class OnlineActivity extends Activity {
	
	public static Activity activity;

	private Button bTrain;
	private Button bClassify;
	private ProgressBar pb;
	public static TextView textViewResult;
	
	private boolean started = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.online);
		activity = this;
		
		pb = (ProgressBar) findViewById(R.id.progressBar1);
		
		bTrain = (Button) findViewById(R.id.buttonTrain);
		bTrain.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlfControler.trainSVM();
			}
		});
		
		bClassify = (Button) findViewById(R.id.buttonClassify);
		bClassify.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!started) {
					started = true;
					pb.setVisibility(1);
					
				} else {
					started = false;
					pb.setVisibility(4);
					AlfControler.classify();
				}
				AlfControler.setStarted(started);
			}
		});

		textViewResult = (TextView) findViewById(R.id.textViewResult);
	}
	

}
