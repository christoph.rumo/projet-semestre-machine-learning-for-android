package ch.eiafr.alf.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import ch.eiafr.alf.R;
import ch.eiafr.alf.controler.AlfControler;

public class MainActivity extends Activity {

	public static AlfControler alfControler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		alfControler = new AlfControler(this.getApplicationContext());
		
		
		final Button bTraining = (Button) findViewById(R.id.bTraining);
		bTraining.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlfControler.trainingMode = true;
				Intent intent = new Intent(MainActivity.this, TrainingActivity.class);
				startActivity(intent);
			}
		});
		
		final Button bOnline = (Button) findViewById(R.id.bOnline);
		bOnline.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlfControler.trainingMode = false;
				Intent intent = new Intent(MainActivity.this, OnlineActivity.class);
				startActivity(intent);
			}
		});
	}
}
