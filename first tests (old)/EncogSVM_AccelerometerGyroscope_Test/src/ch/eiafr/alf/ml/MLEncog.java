package ch.eiafr.alf.ml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.encog.ConsoleStatusReportable;
import org.encog.Encog;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.svm.SVM;
import org.encog.ml.svm.training.SVMSearchTrain;
import org.encog.util.csv.CSVFormat;
import org.encog.util.normalize.DataNormalization;
import org.encog.util.normalize.input.InputField;
import org.encog.util.normalize.input.InputFieldCSV;
import org.encog.util.normalize.output.OutputFieldRangeMapped;
import org.encog.util.normalize.target.NormalizationStorageCSV;
import org.encog.util.simple.TrainingSetUtil;

import ch.eiafr.alf.activity.OnlineActivity;
import ch.eiafr.alf.controler.AlfControler;
import ch.eiafr.alf.ml.data.CSVManager;

public class MLEncog {

	private CSVManager csvManager;
	private SVM svm;

	private final int INPUT = VALUES_PER_RECORD * WINDOWS_SIZE
			* RECORD_PER_WINDOWS; // number of features

	public final static int VALUES_PER_RECORD = 3;
	public final static int RECORD_PER_WINDOWS = 2;
	public final static int WINDOWS_SIZE = 1; // maximal number of measures per
												// window

	public static final int WALK = 1;
	public static final int RUN = 2;
	public static final int STAY = 3;

	public MLEncog(CSVManager csvManager) {
		this.csvManager = csvManager;
	}

	public void normalizeData() {
		try {
			File rawFile = new File(csvManager.getTrainingPath());

			DataNormalization norm = new DataNormalization();
			// InputFieldCSVText inputClass;
			ArrayList<InputField> inputList = new ArrayList<InputField>();

			int i = 0;
			for (; i < INPUT; i++) {
				InputFieldCSV input = new InputFieldCSV(false, rawFile, i);
				inputList.add(input);
			}
			// norm.addInputField(inputClass = new InputFieldCSVText(false,
			// rawFile, i));

			// define how we should normalize
			i = 0;
			for (; i < INPUT; i++) {
				norm.addOutputField(new OutputFieldRangeMapped(
						inputList.get(i), 0, 1));
			}
			// norm.addOutputField(new OutputOneOf(inputClass, 1, 0));

			// define where the output should go
			File outputFile = new File(csvManager.getNormalizedTrainingPath());
			norm.setCSVFormat(CSVFormat.DECIMAL_POINT);
			norm.setTarget(new NormalizationStorageCSV(CSVFormat.DECIMAL_POINT,
					outputFile));

			// process
			norm.setReport(new ConsoleStatusReportable());
			norm.process();
			System.out.println("Output written to: " + rawFile.getPath());

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void trainSVM() throws IOException {

		final MLDataSet trainingSet = TrainingSetUtil.loadCSVTOMemory(
				CSVFormat.ENGLISH, csvManager.getTrainingPath(), false, INPUT,
				1);
		// final MLDataSet trainingSet = TrainingSetUtil.loadCSVTOMemory(
		// CSVFormat.ENGLISH, csvManager.getNormalizedTrainingPath(), false,
		// INPUT, 1);

		// Example Extract one row:
		// MLDataPair result=((BasicMLDataSet)trainingSet).getData().get(0);

		// Creation of the SVM
		svm = new SVM(INPUT, false);

		// TRAIN the SVMs
		SVMSearchTrain train = new SVMSearchTrain(svm, trainingSet);

		int epoch = 1;
		double errorDelta = 0;
		do {
			errorDelta = train.getError();
			train.iteration();
			System.out
					.println("Epoch #" + epoch + " Error:" + train.getError());
			epoch++;
			// && errorDelta-train.getError()!=0
		} while (train.getError() > 0.01);

		if (errorDelta - train.getError() == 0) {
			OnlineActivity.textViewResult
					.setText("Not able to finish the train error:"
							+ train.getError());
			System.out.println("Not able to finish the train error:"
					+ train.getError());
		}

	}

	public void classify() throws IOException {
		System.out.println("Start Classify");
		final MLDataSet onlineSet = TrainingSetUtil.loadCSVTOMemory(
				CSVFormat.ENGLISH, csvManager.getOnlinePath(), false, INPUT, 0);

		for (MLDataPair pair : onlineSet) {
			final MLData output = svm.compute(pair.getInput());
			System.out.println(pair.getInput().getData(0) + ", actual="
					+ output.getData(0));

			int kind = (int) output.getData(0);
			switch (kind) {
			case RUN:
				OnlineActivity.textViewResult.setText("run");
				break;
			case WALK:
				OnlineActivity.textViewResult.setText("walk");
				break;
			case STAY:
				OnlineActivity.textViewResult.setText("stay");
				break;
			default:
				OnlineActivity.textViewResult.setText("ERROR");
				break;
			}
		}
		Encog.getInstance().shutdown();
		System.out.println("Finish Classify");
	}

	public void testTrainingFile() {
		System.out.println("Start test Training File");
		final MLDataSet trainingSet = TrainingSetUtil.loadCSVTOMemory(
				CSVFormat.ENGLISH, csvManager.getTrainingPath(), false, INPUT,
				1);
		System.out.println("SVM Results:");
		for (MLDataPair pair : trainingSet) { // he a "classificationSet" or the
												// online data should be used
			final MLData output = svm.compute(pair.getInput());
			System.out.println(pair.getInput().getData(0) + ","
					+ pair.getInput().getData(1) + ", actual="
					+ output.getData(0) + ",ideal="
					+ pair.getIdeal().getData(0));
		}
		System.out.println("Finish test Training File");
	}
}
