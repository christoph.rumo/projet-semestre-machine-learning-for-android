package ch.eiafr.alf.ml.data;

import ch.eia.fr.ae_framework.Constants;
import ch.eiafr.alf.controler.AlfControler;
import ch.eiafr.alf.ml.MLEncog;
import ch.eiafr.alf.ml.data.bean.OnlineDataBean;
import ch.eiafr.alf.ml.data.bean.TrainingsDataBean;

public class RecordManager {

	public final static int MAX_WINDOWS_BEFORE_WRITE = 100;

	private TrainingsDataBean trainingsData = new TrainingsDataBean();
	private OnlineDataBean onlineData = new OnlineDataBean();

	private boolean started = false; // true if capture is started
	private int kindOfMove = 0; // kind of kindOfMove: 1-walk 2-run 3-stay

	String currentRecord = "";
	boolean isToAccel = true;
	int countRecord = 0;
	int countWindows = 0;

	public void update(Object output, String nSensor) {
		if (started) {
			if (isToAccel && nSensor.equals(Constants.ACCELEROMETER)) {
				if (countRecord != (MLEncog.WINDOWS_SIZE * MLEncog.RECORD_PER_WINDOWS) - 1
						&& countRecord != 0) {
					currentRecord += ",";
				}
				currentRecord += output.toString();
				isToAccel = false;
				countRecord++;
			} else if (!isToAccel && nSensor.equals(Constants.GYROSCOPE)) {
				currentRecord += ",";
				currentRecord += output.toString();
				isToAccel = true;
				countRecord++;
			}
			if (countRecord == MLEncog.WINDOWS_SIZE * MLEncog.RECORD_PER_WINDOWS) {
				if (AlfControler.trainingMode) {
					trainingsData.addTraining(currentRecord, kindOfMove);
					System.out.println("Add windows, count:" + countWindows
							+ " of " + MAX_WINDOWS_BEFORE_WRITE);
					countWindows++;
					if (countWindows >= MAX_WINDOWS_BEFORE_WRITE) {
						System.out
								.println("Max buffer reach! Add training data into file");
						AlfControler.saveTrainingDatasIntoFile();
						countWindows = 0;
					}
				} else {
					onlineData.addOnlineData(currentRecord);
				}
				countRecord = 0;
				currentRecord = "";
				if (!AlfControler.trainingMode) {
					AlfControler.writeOnlineData();
					AlfControler.classify();
				}
			}
		}
	}

	public void resetTrainingData() {
		trainingsData = new TrainingsDataBean();
	}

	public void resetOnlineData() {
		onlineData = new OnlineDataBean();
	}

	public TrainingsDataBean getTrainingsData() {
		return trainingsData;
	}

	public OnlineDataBean getOnlineData() {
		return onlineData;
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public void setKindMove(int move) {
		this.kindOfMove = move;
	}

	public int getCountWindows() {
		return countWindows;
	}

}
