package ch.eiafr.alf.ml.data;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.content.Context;
import ch.eiafr.alf.ml.MLEncog;
import ch.eiafr.alf.ml.data.bean.OnlineDataBean;
import ch.eiafr.alf.ml.data.bean.TrainingsDataBean;

public class CSVManager {

	private String normalizedTrainingPath;
	private String onlinePath;
	private String trainingPath;

	private BufferedWriter bw;
	private FileWriter fw;

	public CSVManager(Context applicationContext) {
		onlinePath = applicationContext.getExternalCacheDir()
				+ "/generated_onlineData.csv";
		trainingPath = applicationContext.getExternalCacheDir()
				+ "/generated_trainingData.csv";
		normalizedTrainingPath = applicationContext.getExternalCacheDir()
				+ "/generated_normalizedTrainingData.csv";
	}

	public void saveTrainingIntoFile(TrainingsDataBean trainingsData)
			throws IOException {
		File file = new File(trainingPath);

		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		fw = new FileWriter(file.getAbsoluteFile(), true);
		bw = new BufferedWriter(fw);

		for (int i = 0; i < trainingsData.getTrainings(MLEncog.WALK).size(); i++) {
			bw.write(trainingsData.getTraining(i, MLEncog.WALK) + ",1");
			bw.newLine();
		}
		System.out.println("write walk");
		for (int i = 0; i < trainingsData.getTrainings(MLEncog.RUN).size(); i++) {
			bw.write(trainingsData.getTraining(i, MLEncog.RUN) + ",2");
			bw.newLine();

		}
		System.out.println("write run");
		for (int i = 0; i < trainingsData.getTrainings(MLEncog.STAY).size(); i++) {
			bw.write(trainingsData.getTraining(i, MLEncog.STAY) + ",3");
			bw.newLine();

		}
		System.out.println("write stay");
		bw.close();
	}

	public void saveOnlineDataIntoFile(OnlineDataBean onlineData)
			throws IOException {
		File file = new File(onlinePath);
		file.createNewFile();

		fw = new FileWriter(file.getAbsoluteFile());
		bw = new BufferedWriter(fw);

		for (int i = 0; i < onlineData.getOnlineDatas().size(); i++) {
			bw.write("" + onlineData.getOnlineData(i));
			bw.newLine();
		}
		bw.close();
	}

	public String getNormalizedTrainingPath() {
		return normalizedTrainingPath;
	}

	public String getOnlinePath() {
		return onlinePath;
	}

	public String getTrainingPath() {
		return trainingPath;
	}

}
