package ch.eiafr.alf.ml.data.bean;

import java.util.ArrayList;

public class OnlineDataBean {

	private ArrayList<String> data = new ArrayList<String>();

	public void addOnlineData(String currentRecord) {
		data.add(currentRecord);
	}

	public String getOnlineData(int index) {
		return data.get(index);
	}

	public ArrayList<String> getOnlineDatas() {
		return data;
	}
}
