package ch.eiafr.alf.ml.data.bean;

import java.util.ArrayList;

public class TrainingsDataBean {
	
	private ArrayList<String> lpbWalk = new ArrayList<String>();
	private ArrayList<String> lpbRun = new ArrayList<String>();
	private ArrayList<String> lpbStay = new ArrayList<String>();
	
	public void addTraining(String currentRecord, int move){
		switch(move){
		case 1: lpbWalk.add(currentRecord);
		break;
		case 2: lpbRun.add(currentRecord);
		break;
		case 3: lpbStay.add(currentRecord);
		break;
		}
	}
	
	public String getTraining(int index, int kindMove){
		switch(kindMove){
		case 1: return lpbWalk.get(index);
		case 2: return lpbRun.get(index);
		case 3: return lpbStay.get(index);
		default : return null;
		}
	}
	
	public ArrayList<String> getTrainings(int kindMove){
		switch(kindMove){
		case 1: return lpbWalk;
		case 2: return lpbRun;
		case 3: return lpbStay;
		default : return null;
		}
	}
	
/*	public String toString(int move){
		String result ="";
		switch(move){
		case 1: for(int i=0;i<lpbWalk.size();i++){
					result += lpbWalk.get(i).toString();
				} 
		case 2: for(int i=0;i<lpbRun.size();i++){
					result += lpbRun.get(i).toString();
				}
		case 3: for(int i=0;i<lpbStay.size();i++){
					result += lpbStay.get(i).toString();
				}
		}
		
		return result;
	}*/
	
}
